import numpy as np
import pylab as p
import pylab
import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation

m1=3
m2=5
l=2
k=5
g=9.81
u11=np.array([0,0,1,0])
u21=np.array([-40,50,-30,30])

MM=np.array([[m1,0,0,0],[0,m1,0,0],[0,0,m2,0],[0,0,0,m2]])

F=np.array([0,-g*m1,0,-g*m2])

x1=[0]
x2=[0]
y1=[0]
y2=[0]

for x in range (1,10000):
    dy=u11[0]-u11[2]
    dx=u11[1]-u11[3]

    if (dx)==0 and dy<=0:
        angle=math.pi/2
        c=math.cos(angle)
        s=math.sin(angle)
    elif (dx)==0 and dy>=0:
        angle=math.pi
        c=math.cos(angle)
        s=math.sin(angle)
    else:
        tan=(dy)/(dx)
        angle= math.atan(tan)
        c=math.cos(angle)
        s=math.sin(angle)

    Kp=np.array([[c*c,c*s,-c*c,-c*s], [s*c,s*s,-s*c,-s*s],[-c*c,-c*s,c*c,c*s],[-s*c,-s*s,s*c,s*s]])
    K=k*Kp

    MMO=np.linalg.inv(MM) 
  
    KV=K@u11

    FK=np.subtract(F,KV)
  
    IL=MMO@FK

    dt=0.1
    u22=IL*dt+u21
    u12=u21*dt+u11

    x1[0]=0
    x2[0]=0
    y1[0]=0
    y2[0]=0
   
    x1.append(u22[0])
    y1.append(u12[1])
    x2.append(u22[2])
    y2.append(u22[3])
    
    u11=u12
    u21=u22

    if u11[1]<0 or u11[3]<0:
        break

p.ion()
fig = p.figure()
ax = fig.add_subplot()

for i in range(len(x1)):

    if i > 0:
        ax.plot(x1[i-1:i+1],y1[i-1:i+1],"green")
        ax.plot(x2[i-1:i+1],y2[i-1:i+1],"red")

    p.draw()
    p.pause(0.01)






